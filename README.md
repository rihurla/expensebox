![Icon](https://bitbucket.org/rihurla/expensebox/raw/b6b1dda5bddd7735bdac449987ac05caa27f8ac2/readme_imgs/icon_readme.png)
# ExpenseBox
App created to control expenses. :)  

## App

### Prerequisites
Xcode and Simulator/iOS Device  
Cocoapods  

### Installing

run `pod install` to include the dependencies  

### Dependencies
Alamofire - https://github.com/Alamofire/Alamofire  
CodableAlamofire - https://github.com/Otbivnoe/CodableAlamofire  

### Running Tests
To run the tests on Xcode, select the ExpenseBox target and device, and click on the Test option or press ⌘+U  

### ScreenShots
![ScreenShot](https://bitbucket.org/rihurla/expensebox/raw/b6b1dda5bddd7735bdac449987ac05caa27f8ac2/readme_imgs/screenshot_readme.png)

## Author
* **Ricardo Hurla** - [Portfolio](https://rihurla.com)  -  [BitBucket](https://bitbucket.org/rihurla/)

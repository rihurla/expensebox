//
//  TransactionDateHelperTests.swift
//  ExpenseBoxTests
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ExpenseBox

class TransactionDateHelperTests: XCTestCase {
  
  let unitUnderTest = TransactionGroupHelper()
  var transactions = [Transaction]()
  var manuallyOrderedTransactions = [Transaction]()
  var transactionGroups = [TransactionGroup]()
  var manuallyOrderedTransactionGroups = [TransactionGroup]()
  
  override func setUp() {
    super.setUp()
    
    let date = Date()
    let date2 = Calendar.current.date(byAdding: .hour, value: 3, to: date)
    let date3 = Calendar.current.date(byAdding: .day, value: 1, to: date)
    let date4 = Calendar.current.date(byAdding: .day, value: 2, to: date)

    let location = TransactionLocation(latitude: "-1111", longitude: "22222")
    let transaction = Transaction(amount: "10.0",
                                  authorisationDate: date,
                                  description: "Test",
                                  location: location,
                                  postTransactionBalance: "10.0",
                                  settlementDate: date)
    
    let transaction2 = Transaction(amount: "10.0",
                                   authorisationDate: date2!,
                                   description: "Test 2",
                                   location: location,
                                   postTransactionBalance: "10.0",
                                   settlementDate: date2!)
    
    let transaction3 = Transaction(amount: "10.0",
                                   authorisationDate: date3!,
                                   description: "Test 3",
                                   location: location,
                                   postTransactionBalance: "10.0",
                                   settlementDate: date3!)
    
    let transaction4 = Transaction(amount: "10.0",
                                   authorisationDate: date4!,
                                   description: "Test 3",
                                   location: location,
                                   postTransactionBalance: "10.0",
                                   settlementDate: date4!)
    
    transactions = [transaction3, transaction, transaction4, transaction2]
    manuallyOrderedTransactions = [transaction4, transaction3, transaction2, transaction]
    
    
    let group = TransactionGroup(date: date, transactions: transactions)
    let group2 = TransactionGroup(date: date2!, transactions: transactions)
    let group3 = TransactionGroup(date: date3!, transactions: transactions)
    let group4 = TransactionGroup(date: date4!, transactions: transactions)
    
    transactionGroups = [group2, group4, group, group3]
    manuallyOrderedTransactionGroups = [group4, group3, group2, group]
  }
  
  override func tearDown() {
    super.tearDown()
    transactions.removeAll()
    manuallyOrderedTransactions.removeAll()
    manuallyOrderedTransactionGroups.removeAll()
  }

  func testTransactionDateOrder() {
    let expectedArray = manuallyOrderedTransactions
    let orderedTransactions = unitUnderTest.orderTransactionsByDescendingDate(transaction: transactions)
    XCTAssertEqual(orderedTransactions, expectedArray)
  }
  
  func testTransactionGroupDateOrder() {
    let expectedArray = manuallyOrderedTransactionGroups
    let orderedTransactionGroups = unitUnderTest.orderTransactionGroupByDescendingDate(transactionGroup: transactionGroups)
    XCTAssertEqual(orderedTransactionGroups, expectedArray)
  }
  
  func testTransactionGroups() {
    let expectedGroupsCount = 3
    let transactionGroups = unitUnderTest.groupTransactionsFrom(transaction: transactions)
    XCTAssertEqual(transactionGroups.count, expectedGroupsCount)
  }
  
}


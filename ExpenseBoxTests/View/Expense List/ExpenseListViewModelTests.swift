//
//  ExpenseListViewModelTests.swift
//  ExpenseBoxTests
//
//  Created by Ricardo Hurla on 15/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ExpenseBox

class ExpenseListViewModelTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testLoadTransactions() {
    let expectedGroupCount = 1
    let expectedTransactionsCount = 2
    let unitUnderTest = ExpenseListViewModel(serviceManager: MockedServiceManager())
    unitUnderTest.loadTransactions()
    let loadedCount = unitUnderTest.transactionGroups.count
    XCTAssertEqual(loadedCount, expectedGroupCount)
    let loadedTransactions = unitUnderTest.transactionGroups.first?.transactions.count
    XCTAssertEqual(loadedTransactions, expectedTransactionsCount)
  }

}

//
//  MockedServiceManager.swift
//  ExpenseBoxTests
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
@testable import ExpenseBox

class MockedServiceManager: NSObject, ServiceManaging {
  func requestTransactions(successHandler: @escaping ([Transaction]) -> Void, errorHandler: @escaping (Error) -> Void) {
    let date = Date()
    let location = TransactionLocation(latitude: "-1111", longitude: "22222")
    let transaction = Transaction(amount: "10.0",
                                  authorisationDate: date,
                                  description: "Test",
                                  location: location,
                                  postTransactionBalance: "10.0",
                                  settlementDate: date)
    
    let transaction2 = Transaction(amount: "10.0",
                                   authorisationDate: date,
                                   description: "Test 2",
                                   location: location,
                                   postTransactionBalance: "10.0",
                                   settlementDate: date)
    
    successHandler([transaction, transaction2])
  }
}

//
//  TransactionLocationTests.swift
//  ExpenseBoxTests
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ExpenseBox

class TransactionLocationTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }

  func testTransactionLocationModel() {
    let expectedLatitude = "-1111"
    let expectedLongitude = "22222"
    let unitUnderTest = TransactionLocation(latitude: expectedLatitude, longitude: expectedLongitude)
    XCTAssertEqual(unitUnderTest.latitude, expectedLatitude)
    XCTAssertEqual(unitUnderTest.longitude, expectedLongitude)
  }

}

//
//  TransactionTests.swift
//  ExpenseBoxTests
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ExpenseBox

class TransactionTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testTransactionModel() {
    let expectedAmount = "10.00"
    let expectedDate = Date()
    let expectedDescription = "Marks & Spencer"
    let expectedLocation = TransactionLocation(latitude: "-1111", longitude: "22222")
    
    let unitUnderTest = Transaction(amount: expectedAmount,
                                    authorisationDate: expectedDate,
                                    description: expectedDescription,
                                    location: expectedLocation,
                                    postTransactionBalance: expectedAmount,
                                    settlementDate: expectedDate)
    
    XCTAssertEqual(unitUnderTest.amount, expectedAmount)
    XCTAssertEqual(unitUnderTest.description, expectedDescription)
    XCTAssertEqual(unitUnderTest.location, expectedLocation)
    XCTAssertEqual(unitUnderTest.postTransactionBalance, expectedAmount)
    XCTAssertEqual(unitUnderTest.authorisationDate, expectedDate)
    XCTAssertEqual(unitUnderTest.settlementDate, expectedDate)
  }

}

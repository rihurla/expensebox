//
//  TransactionGroupTests.swift
//  ExpenseBoxTests
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ExpenseBox

class TransactionGroupTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testTransactionLocationModel() {
    let expectedTransactionCount = 1
    let expectedDate = Date()
    let location = TransactionLocation(latitude: "-1111", longitude: "22222")
    let transaction = Transaction(amount: "10.0",
                                  authorisationDate: expectedDate,
                                  description: "Test",
                                  location: location,
                                  postTransactionBalance: "10.0",
                                  settlementDate: expectedDate)
    let unitUnderTest = TransactionGroup(date: expectedDate, transactions: [transaction])
    XCTAssertEqual(unitUnderTest.transactions.count, expectedTransactionCount)
    XCTAssertEqual(unitUnderTest.date, expectedDate)
  }

}

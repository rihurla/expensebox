//
//  ServiceManagerTests.swift
//  ExpenseBoxTests
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ExpenseBox

class ServiceManagerTests: XCTestCase {
  
  let unitUnderTest: ServiceManaging = MockedServiceManager()
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testRequestTransactions() {
    let requestExpectation = self.expectation(description: "Request")
    unitUnderTest.requestTransactions(successHandler: { (transactions) in
      XCTAssertEqual(transactions.count, 2)
      requestExpectation.fulfill()
    }) { (error) in
      XCTFail(error.localizedDescription)
    }
    self.wait(for: [requestExpectation], timeout: 5.0)
  }
  
}

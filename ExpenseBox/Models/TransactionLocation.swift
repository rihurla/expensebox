//
//  TransactionLocation.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct TransactionLocation: Codable, Equatable {
  let latitude: String
  let longitude: String
}

extension TransactionLocation {
  static func == (lhs: TransactionLocation, rhs: TransactionLocation) -> Bool {
    return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
  }
}

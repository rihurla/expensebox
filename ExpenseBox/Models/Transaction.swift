//
//  Transaction.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct Transaction: Codable, Equatable {
  let amount: String
  let authorisationDate: Date
  let description: String
  let location: TransactionLocation?
  let postTransactionBalance: String
  let settlementDate: Date
  
}

extension Transaction {
  static func == (lhs: Transaction, rhs: Transaction) -> Bool {
    return lhs.amount == rhs.amount &&
      lhs.authorisationDate == rhs.authorisationDate &&
      lhs.postTransactionBalance == rhs.postTransactionBalance &&
      lhs.settlementDate == rhs.settlementDate
  }
}


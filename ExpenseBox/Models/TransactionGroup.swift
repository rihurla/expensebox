//
//  GroupedTransactions.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct TransactionGroup: Equatable {
  let date: Date
  let transactions: [Transaction]
}

extension TransactionGroup {
  static func == (lhs: TransactionGroup, rhs: TransactionGroup) -> Bool {
    return lhs.date == rhs.date
  }
}

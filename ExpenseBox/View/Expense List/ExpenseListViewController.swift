//
//  ViewController.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class ExpenseListViewController: UIViewController {
  
  private let defaultErrorTitle = NSLocalizedString("alert_error_title", comment: "error title")
  private let viewModel: ExpenseListViewModel
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  init(viewModel: ExpenseListViewModel) {
    self.viewModel = viewModel
    super.init(nibName: "ExpenseListViewController", bundle: Bundle.main)
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = NSLocalizedString("expenses_list_vc_title", comment: "list title")
    activityIndicator.hidesWhenStopped = true
    activityIndicator.startAnimating()
    viewModel.delegate = self
    viewModel.loadTransactions()
    configureTableView()
    configureAppIcon()
  }
  
  private func configureAppIcon() {
    self.navigationController?.configureNavBarLeftIconFor(viewController: self)
  }

  private func configureTableView() {
    tableView.delegate = viewModel
    tableView.dataSource = viewModel
    tableView.estimatedRowHeight = 55
    tableView.rowHeight = UITableView.automaticDimension
    tableView.tableFooterView = UIView()
    let dateHeaderNib = UINib(nibName: "DateHeaderView", bundle: Bundle.main)
    tableView.register(dateHeaderNib, forHeaderFooterViewReuseIdentifier: DateHeaderView.dateHeaderReusableIdentifier)
    let transactionCellNib = UINib(nibName: "TransactionTableViewCell", bundle: nil)
    tableView.register(transactionCellNib, forCellReuseIdentifier: TransactionTableViewCell.transactionCellIdentifier)
  }
  
  private func refresh() {
    tableView.reloadData()
    activityIndicator.stopAnimating()
  }
  
  private func displayErrorWith(title: String, message: String) {
    activityIndicator.stopAnimating()
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString("alert_error_option", comment: "error ok option"),
                                  style: .default, handler: { action in }))
    self.present(alert, animated: true, completion: nil)
  }

}

extension ExpenseListViewController: ExpenseListViewModelDelegate {
  
  func viewModel(_ viewModel: ExpenseListViewModel, willPresentLocationWith title: String, and location: TransactionLocation) {
    let transactionLocationViewModel = TransactionLocationViewModel(title: title, location: location)
    let transactionLocationViewController = TransactionLocationViewController(viewModel: transactionLocationViewModel)
    self.navigationController?.pushViewController(transactionLocationViewController, animated: true)
  }
  
  func viewModelDidFinishRequestWithSuccess(_ viewModel: ExpenseListViewModel) {
    refresh()
  }
  
  func viewModel(_ viewModel: ExpenseListViewModel, didFinishRequestWith error: ExpenseBoxError) {
    displayErrorWith(title: NSLocalizedString("alert_error_title", comment: "error title"),
                     message: error.localizedDescription)
  }
  
  func viewModel(_ viewModel: ExpenseListViewModel, didFailLocationWith error: ExpenseBoxError) {
    displayErrorWith(title: NSLocalizedString("alert_location_title", comment: "location title"),
                     message: error.localizedDescription)
  }
}


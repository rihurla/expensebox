//
//  DateHeaderView.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class DateHeaderView: UITableViewHeaderFooterView {
  
  static let dateHeaderReusableIdentifier = "DateHeaderView"
  static let headerHeight: CGFloat = 40.0
  
  @IBOutlet weak var dateLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundView = UIView(frame: self.bounds)
    self.backgroundView?.backgroundColor = ColorPalette.headerBackground
  }
  
  func setUp(date: String) {
    dateLabel.text = date
  }
  
}

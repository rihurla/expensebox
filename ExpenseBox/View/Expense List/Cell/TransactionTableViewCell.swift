//
//  TransactionTableViewCell.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
  
  static let transactionCellIdentifier = "TransactionCell"
  @IBOutlet weak var amountLabel: UILabel!
  @IBOutlet weak var transactionDescriptionLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func setUp(transaction: Transaction) {
    if let amount = Double(transaction.amount) {
      amountLabel.textColor = amount > 0 ? ColorPalette.valueGreen : ColorPalette.valueRed
      let format = NSLocalizedString("amount_value_format", comment: "amount format")
      amountLabel.text = String.localizedStringWithFormat(format, amount)
    } else {
      amountLabel.textColor = ColorPalette.valueRed
      amountLabel.text = transaction.amount
    }
    transactionDescriptionLabel.text = transaction.description
  }
    
}

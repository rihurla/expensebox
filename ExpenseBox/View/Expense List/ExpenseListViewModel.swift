//
//  ExpenseListViewModel.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

protocol ExpenseListViewModelDelegate: class {
  func viewModelDidFinishRequestWithSuccess(_ viewModel: ExpenseListViewModel)
  func viewModel(_ viewModel: ExpenseListViewModel, didFinishRequestWith error: ExpenseBoxError)
  func viewModel(_ viewModel: ExpenseListViewModel, didFailLocationWith error: ExpenseBoxError)
  func viewModel(_ viewModel: ExpenseListViewModel, willPresentLocationWith title: String, and location: TransactionLocation)
}

class ExpenseListViewModel: NSObject {
  
  weak var delegate: ExpenseListViewModelDelegate?
  private let serviceManager: ServiceManaging
  private(set) var transactionGroups = [TransactionGroup]()
  private let formatter = DateFormatter()
  private let transactionHelper = TransactionGroupHelper()
  
  init(serviceManager: ServiceManaging) {
    self.serviceManager = serviceManager
    super.init()
    formatter.dateStyle = .medium
  }
  
  func loadTransactions() {
    serviceManager.requestTransactions(successHandler: { (transactions) in
      self.transactionGroups = self.organizedTransactionGroupsFrom(transactionList: transactions)
      self.delegate?.viewModelDidFinishRequestWithSuccess(self)
    }) { (error) in
      self.delegate?.viewModel(self, didFinishRequestWith: ExpenseBoxError(error.localizedDescription))
    }
  }
  
  private func organizedTransactionGroupsFrom(transactionList: [Transaction]) -> [TransactionGroup] {
    return transactionHelper.groupTransactionsFrom(transaction: transactionList)
  }
  
}

extension ExpenseListViewModel: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let transaction = transactionGroups[indexPath.section].transactions[indexPath.item]
    if let location = transaction.location {
      self.delegate?.viewModel(self, willPresentLocationWith: transaction.description, and: location)
    } else {
      let errorMsg = NSLocalizedString("location_not_found_error", comment: "location error")
      self.delegate?.viewModel(self, didFailLocationWith: ExpenseBoxError(errorMsg))
    }
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let identifier = DateHeaderView.dateHeaderReusableIdentifier
    let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: identifier) as! DateHeaderView
    headerView.setUp(date: formatter.string(from: transactionGroups[section].date))
    return headerView
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return DateHeaderView.headerHeight
  }
  
}

extension ExpenseListViewModel: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let identifier = TransactionTableViewCell.transactionCellIdentifier
    let transaction = transactionGroups[indexPath.section].transactions[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! TransactionTableViewCell
    cell.setUp(transaction: transaction)
    cell.selectionStyle = .none
    return cell
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return transactionGroups.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return transactionGroups[section].transactions.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
}

//
//  TransactionLocationViewController.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit
import MapKit

class TransactionLocationViewController: UIViewController {
  
  private let viewModel: TransactionLocationViewModel
  private let transactionLocationAnnotation = MKPointAnnotation()
  @IBOutlet weak var mapView: MKMapView!
  
  init(viewModel: TransactionLocationViewModel) {
    self.viewModel = viewModel
    super.init(nibName: "TransactionLocationViewController", bundle: Bundle.main)
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = viewModel.transactionTitle
    placePin()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    mapView.removeAnnotation(transactionLocationAnnotation)
  }

  private func placePin() {
    if let latitude = Double(viewModel.transactionLocation.latitude), let longitude = Double(viewModel.transactionLocation.longitude) {
      transactionLocationAnnotation.title = viewModel.transactionTitle
      transactionLocationAnnotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
      mapView.addAnnotation(transactionLocationAnnotation)
      zoomTo(latitude: latitude, longitude: longitude)
    }
  }
  
  private func zoomTo(latitude: Double, longitude: Double) {
    let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
    mapView.setRegion(region, animated: true)
  }

}

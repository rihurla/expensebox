//
//  TransactionLocationViewModel.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

class TransactionLocationViewModel: NSObject {
  
  let transactionTitle: String
  let transactionLocation: TransactionLocation
  
  init(title: String, location: TransactionLocation) {
    self.transactionTitle = title
    self.transactionLocation = location
    super.init()
  }
}

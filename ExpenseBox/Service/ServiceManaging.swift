//
//  ServiceManaging.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

protocol ServiceManaging {
  func requestTransactions(successHandler: @escaping ([Transaction]) -> Void, errorHandler: @escaping (Error) -> Void)
}

//
//  ServiceUrls.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
/**
 Api urls
 */
struct ServiceUrls {
  static let baseUrl = "http://private-710eeb-lootiosinterview.apiary-mock.com"
  static let transactionList = "/transactions"
}

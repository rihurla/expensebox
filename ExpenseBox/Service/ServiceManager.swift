//
//  ServiceManager.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class ServiceManager: NSObject, ServiceManaging {
  
  /**
   Request transactions from the API.
   */
  func requestTransactions(successHandler: @escaping ([Transaction]) -> Void, errorHandler: @escaping (Error) -> Void) {
    
    let urlString = ServiceUrls.baseUrl + ServiceUrls.transactionList
    guard let url = URL(string: urlString) else {
      let messageFormat = NSLocalizedString("request_manager_url_error_format", comment: "format for url error")
      let errorMessage = String.localizedStringWithFormat(messageFormat, urlString)
      errorHandler(ExpenseBoxError(errorMessage))
      return
    }
    
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = .convertFromSnakeCase // Automatically convert snake_case to camelCase
    decoder.dateDecodingStrategy = .iso8601 // Date decoding
    
    Alamofire.request(url).responseDecodableObject(keyPath: nil,
                                                   decoder: decoder) { (response: DataResponse<[Transaction]>) in
                                                    switch response.result {
                                                    case .success:
                                                      if let transactionArray = response.result.value {
                                                        successHandler(transactionArray)
                                                      } else {
                                                        let errorMessage = NSLocalizedString("request_manager_request_error",
                                                                                             comment: "no transactions found error")
                                                        errorHandler(ExpenseBoxError(errorMessage))
                                                      }
                                                    case .failure(let error):
                                                      errorHandler(error)
                                                    }
    }
    
  }
  
}

//
//  AppDelegate.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    window!.rootViewController = configureRootViewController()
    window!.makeKeyAndVisible()
    return true
  }
  
  private func configureRootViewController() -> UINavigationController {
    let serviceManager = ServiceManager()
    let expenseListViewModel = ExpenseListViewModel(serviceManager: serviceManager)
    let expenseListViewController = ExpenseListViewController(viewModel: expenseListViewModel)
    let navigationController = UINavigationController(rootViewController: expenseListViewController)
    navigationController.configureAppearance()
    return navigationController
  }

  func applicationWillResignActive(_ application: UIApplication) {}

  func applicationDidEnterBackground(_ application: UIApplication) {}

  func applicationWillEnterForeground(_ application: UIApplication) {}

  func applicationDidBecomeActive(_ application: UIApplication) {}

  func applicationWillTerminate(_ application: UIApplication) {}


}


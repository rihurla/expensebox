//
//  DateExtensions.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
/**
 Extends the Date functionality to remove time from a date.
 */
extension Date {
  func stripTime() -> Date {
    let components = Calendar.current.dateComponents([.year, .month, .day], from: self)
    let date = Calendar.current.date(from: components)
    return date!
  }
}

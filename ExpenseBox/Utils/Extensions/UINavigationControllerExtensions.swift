//
//  UINavigationControllerExtensions.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 14/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit
/**
 Extends the Navigation Controller functionality to customize appearance.
 */
extension UINavigationController {
  
  func configureAppearance() {
    let navigationBarColor = ColorPalette.navigationBarBackground
    self.navigationBar.barTintColor = navigationBarColor
    self.navigationBar.isTranslucent = false
    self.navigationItem.largeTitleDisplayMode = .always
    self.navigationBar.prefersLargeTitles = true
    self.navigationBar.tintColor = ColorPalette.navigationBarTint
    self.navigationBar.barStyle = UIBarStyle.black
  }
  
  func configureNavBarLeftIconFor(viewController: UIViewController) {
    let image = UIImage(named: "nav_icon")
    let imageView = UIImageView(image: image)
    imageView.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
    let widthConstraint = imageView.widthAnchor.constraint(lessThanOrEqualToConstant: 36)
    let heightConstraint = imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1.0)
    heightConstraint.isActive = true
    widthConstraint.isActive = true
    let barButton = UIBarButtonItem(customView: imageView)
    viewController.navigationItem.leftBarButtonItem = barButton
  }
  
}

//
//  TransactionDateHelper.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct TransactionGroupHelper {
  /**
   Returns an array of transactions groups ordered by date.
   
   - parameter transaction list: List of transactions received from the API.
   */
  func groupTransactionsFrom(transaction list: [Transaction]) -> [TransactionGroup] {
    let datesArray = list.map({ $0.authorisationDate.stripTime() })
    let uniqueDates = Array(Set(datesArray)).sorted()
    var groupedTransactions = [TransactionGroup]()
    
    for date in uniqueDates {
      let group = list.filter({
        return $0.authorisationDate.stripTime() == date
      })
      let orderedGroup = orderTransactionsByDescendingDate(transaction: group)
      let transactionGroup = TransactionGroup(date: date, transactions: orderedGroup)
      groupedTransactions.append(transactionGroup)
    }
    
    return orderTransactionGroupByDescendingDate(transactionGroup: groupedTransactions)
  }
  
  /**
   Returns an array of transactions ordered by date.
   
   - parameter transaction list: List of transactions received from the API.
   */
  func orderTransactionsByDescendingDate(transaction list: [Transaction]) -> [Transaction] {
    return list.sorted(by: {
      $0.authorisationDate.compare($1.authorisationDate) == .orderedDescending
    })
  }
  /**
   Returns an array of transaction groups ordered by date.
   
   - parameter transaction list: List of transactions received from the API.
   */
  func orderTransactionGroupByDescendingDate(transactionGroup list: [TransactionGroup]) -> [TransactionGroup] {
    return list.sorted(by: {
      $0.date.compare($1.date) == .orderedDescending
    })
  }
  
}

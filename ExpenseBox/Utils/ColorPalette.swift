//
//  ColorPalette.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 15/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit
/**
 App colors
 */
struct ColorPalette {
  static let valueRed = UIColor(red: 255/255.0, green: 113/255.0, blue: 91/255.0, alpha: 1.0)
  static let valueGreen = UIColor(red: 120/255.0, green: 224/255.0, blue: 143/255.0, alpha: 1.0)
  static let headerBackground = UIColor(red: 204/255.0, green: 235/255.0, blue: 242/255.0, alpha: 1.0)
  static let navigationBarBackground = UIColor(red: 130/255.0, green: 204/255.0, blue: 221/255.0, alpha: 1.0)
  static let navigationBarTint = UIColor.white
}


//
//  ExpenseBoxError.swift
//  ExpenseBox
//
//  Created by Ricardo Hurla on 10/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
struct ExpenseBoxError: Error {
  
  let message: String
  
  init(_ message: String) {
    self.message = message
  }
  
  public var localizedDescription: String {
    return message
  }
  
}
